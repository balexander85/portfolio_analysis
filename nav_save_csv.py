# coding: utf-8
import os
from time import sleep

from webdriverplus import WebDriver

from packages import util
from auth import user_name, password, account_name

driver = WebDriver('firefox', quit_on_exit=True, reuse_browser=True)


def log_in(driver):
    # find user id box
    user_id_box = util.wait_for_element(driver, xpath='//input[@class="txtBoxLogin"]')
    user_id_box.send_keys(user_name)
    driver.find(xpath='//input[@value=" Continue "]').click()
    # find password box
    password_box = util.wait_for_element(driver, xpath='//input[@id="ctl00_ctl00_ctl00_cph'
                                                       'NestedUtility_cphStage_cpLoginContent_ctl'
                                                       'ValidateUser_ctl00_txtPassword"]')
    # send password
    password_box.send_keys(password)
    util.wait_for_element(driver, xpath='//input[@value="Continue >>"]').click()


def download_csv(driver):
    # go_to_download_screen
    driver.firefox_profile.set_preference("browser.download.folderList", 2)
    driver.firefox_profile.set_preference("browser.download.dir", os.getcwd())
    driver.firefox_profile.set_preference("browser.helperApps.neverAsk.saveToDisk", "application/csv, text/csv")
    util.wait_for_element(driver, text='Download').click()
    util.wait_for_element(driver, xpath='//span[@class="acctSelectorInline"]/span/a').click()
    util.wait_for_element(driver, xpath='//span[text()="{}"]'.format(account_name)).click()
    util.wait_for_element(driver, xpath='//input[@value="Dnld_RadioButtonText"]').click()
    util.wait_for_element(driver, xpath='//input[@value="Holdings"]').click()
    util.wait_for_element(driver, xpath='//span[text()="Comma-Separated Spreadsheet (.CSV) ›"]').click()


def main():
    driver.get('https://olui2.fs.ml.com/login/login.aspx')
    log_in(driver)
    download_dir_files_start = os.listdir('/Users/Brian/Downloads')
    download_csv(driver)
    sleep(2)
    download_dir_files_after = os.listdir('/Users/Brian/Downloads')
    # find the file diff from download dir
    csv_file_name = [x for x in download_dir_files_after if x not in download_dir_files_start][0]
    os.rename('/Users/Brian/Downloads/{}'.format(csv_file_name), 'Holdings_{}.csv'.format(util.todays_date))

    print 'End of Program'
