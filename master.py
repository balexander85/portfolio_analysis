import csv
import re

from packages import util
import nav_save_csv


def open_scrape_insert_db():
    db = util.db
    total_portfolio_value = 0
    with open('Holdings_{}.csv'.format(util.todays_date), 'rb') as csvfile:
        rows_reader = csv.reader(csvfile, delimiter=',', quotechar='"')
        for row in rows_reader:
            if not re.search(r'COB\sDate', row[0]):
                date = row[0].split('/')
                date = '{}-{}-{}'.format(date[2], date[0].zfill(2), date[1].zfill(2))
                security_num = row[1]
                symbol = row[2]
                cusip_num = row[3]
                security_name = row[4]
                security_qty = float(row[8])
                security_price = float(row[9])
                # Given Value
                #security_total_value = float(row[10])
                calculated_value = security_price * security_qty
                total_portfolio_value = total_portfolio_value + calculated_value
                print '|{}| Quantity: {} * Price: ${} = ${}'.format(symbol, security_qty, security_price, calculated_value)
                query_date_data_exist = ('SELECT log_date, security_num, symbol, cusip_num, '
                                         'security_name, security_qty, security_price '
                                         'FROM daily_balances WHERE log_date="{}" AND '
                                         'symbol="{}" AND security_price="{}"'.format(date, symbol, security_price))
                query_insert = ('INSERT INTO daily_balances (log_date, security_num, symbol, cusip_num, security_name, security_qty, security_price) '
                                'VALUES ("{}", "{}", "{}", "{}", "{}", "{}", "{}");'.format(date, security_num, symbol, cusip_num, security_name,
                                                                                            security_qty, security_price))
                if not util.return_data_from_db(db, query_date_data_exist):
                    try:
                        db.query(query_insert)
                        print 'Inserting into db......'
                    except Exception, e:
                        print repr(e)
                else:
                    print 'Today\'s date has already been logged, no databse INSERT....'

        print '---------------------------'
        print 'Total Portfolio Value = $ {}'.format(round(total_portfolio_value, 2))


nav_save_csv.main()
open_scrape_insert_db()
