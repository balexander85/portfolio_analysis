from datetime import datetime

import MySQLdb
from selenium.webdriver.support.ui import WebDriverWait     # available since 2.4.0

from auth import db_host, db_user, db_password

now = datetime.now()
todays_date = '{}{}{}'.format(str(now.month).zfill(2), str(now.day).zfill(2), now.year)

# db = MySQLdb.connect(unix_socket='/Applications/MAMP/tmp/mysql/mysql.sock',
                     # host='localhost', user='root', passwd='root', db='portfolio')

db = MySQLdb.connect(unix_socket='/usr/bin/mysql',
                     host=db_host, user=db_user, passwd=db_password, db='dgs_data')


def return_data_from_db(db, query_select):
    '''Returns data from the database by using SELECT.'''
    cursor = db.cursor()
    cursor.execute(query_select)
    data = cursor.fetchall()
    return data


def wait_for_element(driver, **kwargs):
    '''Uses Selenium"s WebDriverWait, return the element that is found.'''
    for locator, value in kwargs.iteritems():
        args = {locator: value}
        try:
            element = WebDriverWait(driver, 5).until(lambda driver: driver.find(**args))
            return element
        except:
            pass
